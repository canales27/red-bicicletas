var Bicicleta =function (id, color, modelo, ubicacion) {
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}

Bicicleta.prototype.toString=function(){
    return 'id: ' + this.id + "| color: " + this.color;
}

Bicicleta.AllBicis=[];
Bicicleta.add=function(aBici){
    Bicicleta.AllBicis.push(aBici);
}

Bicicleta.findById=function(aBiciId){
    var aBici=Bicicleta.AllBicis.find(x => x.id==aBiciId);
    if(aBiciId)
        return aBiciId;
    else
        throw new Error(`No existe una bici con el id ${aBiciId}`);
}

Bicicleta.removeById=function(aBiciId){
    //var aBici=Bicicleta.findById(aBiciId);
    for(var i=0; i<Bicicleta.AllBicis.length; i++)
        if(Bicicleta.AllBicis[i].id==aBiciId){
            Bicicleta.AllBicis.splice(i, 1);
            break;
        }
}

var a=new Bicicleta(1, 'rojo ',"urbana",[15.5185456,-88.0339430]);
var b=new Bicicleta(2, 'amarillo ',"urbana",[15.5185234,-88.0339559]);
Bicicleta.add(a);
Bicicleta.add(b);

module.exports=Bicicleta;